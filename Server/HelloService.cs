﻿using Generated;
using Grpc.Core;
using System;
using System.Threading.Tasks;

namespace Server
{
    internal class HelloService : Generated.HelloService.HelloServiceBase
    {
        public override Task<HelloResponse> SayHello(HelloRequest request, ServerCallContext context)
        {
            Console.WriteLine("Hello " + request.Name + " " + request.Surname);

            return Task.FromResult(new HelloResponse());
        }
    }
}
