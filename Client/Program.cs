﻿using Generated;
using Grpc.Core;
using System;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            const string Host = "localhost";
            const int Port = 16842;

            var channel = new Channel($"{Host}:{Port}", ChannelCredentials.Insecure);
            var request = new HelloRequest();

            Console.WriteLine("Enter your name ");

            request.Name = Console.ReadLine();

            Console.WriteLine("Enter your surname ");

            request.Surname = Console.ReadLine();

            var client = new HelloService.HelloServiceClient(channel);

            client.SayHello(request);


            // Shutdown
            channel.ShutdownAsync().Wait();
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
